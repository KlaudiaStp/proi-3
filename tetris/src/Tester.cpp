#include "Tester.h"
#include <iostream>

struct Tester::TesterImpl
{
    MyContainer <Point> listOfPoints;
};


Tester::Tester() : impl_(new TesterImpl)
{

}


Tester::~Tester()
{
    //dtor
}

void Tester::runTest()
{
    testTetO();
    testTetI();
    testTetS();
    testTetZ();
    testTetT();
    testTetL();
    testTetJ();
}

void Tester::testTetO()
{
    Point q[4];
    q[0].setXY(0,0);
    q[1].setXY(0,1);
    q[2].setXY(1,0);
    q[3].setXY(1,1);
    Tetrimino* tet = new Tetrimino(TetType(0),0,0);
    Point p[4];
    tet->points(p, false);
    impl_->listOfPoints.add(p[0]);
    impl_->listOfPoints.add(p[1]);
    impl_->listOfPoints.add(p[2]);
    impl_->listOfPoints.add(p[3]);
    //bool ok=true;
    bool ok=(impl_->listOfPoints.contains(q[0]))&&(impl_->listOfPoints.contains(q[1]))&&(impl_->listOfPoints.contains(q[2]))&&(impl_->listOfPoints.contains(q[3]));
    std::cout<<"Test Tetrimino O: "<<ok<<std::endl;
    bool go;
    go=impl_->listOfPoints.first();
    while(go)
    {

        impl_->listOfPoints.removecur();
        go=impl_->listOfPoints.get()!=NULL;
    }
    delete tet;
}


void Tester::testTetI()
{
 Point q[4];
    q[0].setXY(0,0);
    q[1].setXY(0,1);
    q[2].setXY(0,2);
    q[3].setXY(0,3);
    Tetrimino* tet = new Tetrimino(TetType(1),0,0);
    Point p[4];
    tet->points(p, false);
    impl_->listOfPoints.add(p[0]);
    impl_->listOfPoints.add(p[1]);
    impl_->listOfPoints.add(p[2]);
    impl_->listOfPoints.add(p[3]);
    //bool ok=true;
    bool ok=(impl_->listOfPoints.contains(q[0]))&&(impl_->listOfPoints.contains(q[1]))&&(impl_->listOfPoints.contains(q[2]))&&(impl_->listOfPoints.contains(q[3]));
    std::cout<<"Test Tetrimino I: "<<ok<<std::endl;
    bool go;
    go=impl_->listOfPoints.first();
    while(go)
    {

        impl_->listOfPoints.removecur();
        go=impl_->listOfPoints.get()!=NULL;
    }
    delete tet;
}

void Tester::testTetS()
{
 Point q[4];
    q[0].setXY(1,0);
    q[1].setXY(2,0);
    q[2].setXY(0,1);
    q[3].setXY(1,1);
    Tetrimino* tet = new Tetrimino(TetType(2),0,0);
    Point p[4];
    tet->points(p, false);
    impl_->listOfPoints.add(p[0]);
    impl_->listOfPoints.add(p[1]);
    impl_->listOfPoints.add(p[2]);
    impl_->listOfPoints.add(p[3]);
    //bool ok=true;
    bool ok=(impl_->listOfPoints.contains(q[0]))&&(impl_->listOfPoints.contains(q[1]))&&(impl_->listOfPoints.contains(q[2]))&&(impl_->listOfPoints.contains(q[3]));
    std::cout<<"Test Tetrimino S: "<<ok<<std::endl;
    bool go;
    go=impl_->listOfPoints.first();
    while(go)
    {

        impl_->listOfPoints.removecur();
        go=impl_->listOfPoints.get()!=NULL;
    }
     delete tet;
}

void Tester::testTetZ()
{
 Point q[4];
    q[0].setXY(0,0);
    q[1].setXY(1,0);
    q[2].setXY(1,1);
    q[3].setXY(2,1);
    Tetrimino* tet = new Tetrimino(TetType(3),0,0);
    Point p[4];
    tet->points(p, false);
    impl_->listOfPoints.add(p[0]);
    impl_->listOfPoints.add(p[1]);
    impl_->listOfPoints.add(p[2]);
    impl_->listOfPoints.add(p[3]);
    //bool ok=true;
    bool ok=(impl_->listOfPoints.contains(q[0]))&&(impl_->listOfPoints.contains(q[1]))&&(impl_->listOfPoints.contains(q[2]))&&(impl_->listOfPoints.contains(q[3]));
    std::cout<<"Test Tetrimino Z: "<<ok<<std::endl;
    bool go;
    go=impl_->listOfPoints.first();
    while(go)
    {

        impl_->listOfPoints.removecur();
        go=impl_->listOfPoints.get()!=NULL;
    }
     delete tet;
}

void Tester::testTetT()
{
 Point q[4];
    q[0].setXY(0,0);
    q[1].setXY(1,0);
    q[2].setXY(2,0);
    q[3].setXY(1,1);
    Tetrimino* tet = new Tetrimino(TetType(4),0,0);
    Point p[4];
    tet->points(p, false);
    impl_->listOfPoints.add(p[0]);
    impl_->listOfPoints.add(p[1]);
    impl_->listOfPoints.add(p[2]);
    impl_->listOfPoints.add(p[3]);
    //bool ok=true;
    bool ok=(impl_->listOfPoints.contains(q[0]))&&(impl_->listOfPoints.contains(q[1]))&&(impl_->listOfPoints.contains(q[2]))&&(impl_->listOfPoints.contains(q[3]));
    std::cout<<"Test Tetrimino O: "<<ok<<std::endl;
    bool go;
    go=impl_->listOfPoints.first();
    while(go)
    {

        impl_->listOfPoints.removecur();
        go=impl_->listOfPoints.get()!=NULL;
    }
     delete tet;
}

void Tester::testTetL()
{
 Point q[4];
    q[0].setXY(0,0);
    q[1].setXY(0,1);
    q[2].setXY(0,2);
    q[3].setXY(1,2);
    Tetrimino* tet = new Tetrimino(TetType(5),0,0);
    Point p[4];
    tet->points(p, false);
    impl_->listOfPoints.add(p[0]);
    impl_->listOfPoints.add(p[1]);
    impl_->listOfPoints.add(p[2]);
    impl_->listOfPoints.add(p[3]);
    //bool ok=true;
    bool ok=(impl_->listOfPoints.contains(q[0]))&&(impl_->listOfPoints.contains(q[1]))&&(impl_->listOfPoints.contains(q[2]))&&(impl_->listOfPoints.contains(q[3]));
    std::cout<<"Test Tetrimino L: "<<ok<<std::endl;
    bool go;
    go=impl_->listOfPoints.first();
    while(go)
    {

        impl_->listOfPoints.removecur();
        go=impl_->listOfPoints.get()!=NULL;
    }
     delete tet;
}

void Tester::testTetJ()
{
 Point q[4];
    q[0].setXY(1,0);
    q[1].setXY(1,1);
    q[2].setXY(1,2);
    q[3].setXY(0,2);
    Tetrimino* tet = new Tetrimino(TetType(6),0,0);
    Point p[4];
    tet->points(p, false);
    impl_->listOfPoints.add(p[0]);
    impl_->listOfPoints.add(p[1]);
    impl_->listOfPoints.add(p[2]);
    impl_->listOfPoints.add(p[3]);
    //bool ok=true;
    bool ok=(impl_->listOfPoints.contains(q[0]))&&(impl_->listOfPoints.contains(q[1]))&&(impl_->listOfPoints.contains(q[2]))&&(impl_->listOfPoints.contains(q[3]));
    std::cout<<"Test Tetrimino J: "<<ok<<std::endl;
    bool go;
    go=impl_->listOfPoints.first();
    while(go)
    {

        impl_->listOfPoints.removecur();
        go=impl_->listOfPoints.get()!=NULL;
    }
     delete tet;
}
