#include "Tetrimino.h"
#include "Point.h"
#include <ncurses.h>
#define BLOCK ' '|A_REVERSE
#define SPACE ' '

Tetrimino::Tetrimino(TetType type, int x, int y)
{
    myType=type;
    state=0;
    posX=x;
    posY=y;
    maxStates=tabMaxStates[myType];

}


Tetrimino::~Tetrimino()
{
    //dtor
}
void Tetrimino::print0 (bool fillb)
{
    int c = fillb ? BLOCK:SPACE;
    move(posY, posX*2);
    addch(c);
    addch(c);
    addch(c);
    addch(c);
    move(posY+1,posX*2);
    addch(c);
    addch(c);
    addch(c);
    addch(c);

}
void Tetrimino::printTet(bool fillb)
{
    int c = fillb ? BLOCK:SPACE;
    switch (myType)
    {
    case 0:
        print0(fillb);
        break;

    case 1:
        move(posY, posX*2);
        addch(c);
        addch(c);
        if(state==0)
        {
            move(posY+1, posX*2);
            addch(c);
            addch(c);
            move(posY+2, posX*2);
            addch(c);
            addch(c);
            move(posY+3, posX*2);
            addch(c);
            addch(c);
        }
        else if(state==1)
        {
            move(posY, posX*2+2);
            addch(c);
            addch(c);
            move(posY, posX*2+4);
            addch(c);
            addch(c);
            move(posY, posX*2+6);
            addch(c);
            addch(c);
        }
    default:
        move(posY, posX*2);
        if(shapes[myType][state]&1)
        {
            addch(c);
            addch(c);

        }
        if(shapes[myType][state]&2)
        {
            move(posY, posX*2+2);
            addch(c);
            addch(c);
        }
        if(shapes[myType][state]&4)
        {
            move(posY, posX*2+4);
            addch(c);
            addch(c);
        }
        if(shapes[myType][state]&8)
        {
            move(posY+1, posX*2);
            addch(c);
            addch(c);
        }
        if(shapes[myType][state]&16)
        {
            move(posY+1, posX*2+2);
            addch(c);
            addch(c);
        }
        if(shapes[myType][state]&32)
        {
            move(posY+1, posX*2+4);
            addch(c);
            addch(c);
        }
        if(shapes[myType][state]&64)
        {
            move(posY+2, posX*2);
            addch(c);
            addch(c);
        }
        if(shapes[myType][state]&128)
        {
            move(posY+2, posX*2+2);
            addch(c);
            addch(c);
        }

    }

}

void Tetrimino::pointsO(Point p[4], bool rot)
{
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY;
    p[2].x=posX;
    p[2].y=posY+1;
    p[3].x=posX+1;
    p[3].y=posY+1;
}

void Tetrimino::pointsI(Point p[4], bool rot)
{
    int fs=!rot ? state: (state+1)%tabMaxStates[myType];
    switch (fs)
    {
    case 0:
        p[0].x=posX;
        p[0].y=posY;
        p[1].x=posX;
        p[1].y=posY+1;
        p[2].x=posX;
        p[2].y=posY+2;
        p[3].x=posX;
        p[3].y=posY+3;
        break;
    case 1:
        p[0].x=posX;
        p[0].y=posY;
        p[1].x=posX+1;
        p[1].y=posY;
        p[2].x=posX+2;
        p[2].y=posY;
        p[3].x=posX+3;
        p[3].y=posY;
        break;
    }

}

void Tetrimino::pointsS(Point p[4], bool rot)
{
    int fs=!rot ? state: (state+1)%tabMaxStates[myType];
    switch(fs)
    {
    case 0:
        p[0].x=posX+1;
        p[0].y=posY;
        p[1].x=posX+2;
        p[1].y=posY;
        p[2].x=posX;
        p[2].y=posY+1;
        p[3].x=posX+1;
        p[3].y=posY+1;
        break;
    case 1:
        p[0].x=posX;
        p[0].y=posY;
        p[1].x=posX;
        p[1].y=posY+1;
        p[2].x=posX+1;
        p[2].y=posY+1;
        p[3].x=posX+1;
        p[3].y=posY+2;
        break;
    }
}

void Tetrimino::pointsZ(Point p[4], bool rot)
{
     int fs=!rot ? state: (state+1)%tabMaxStates[myType];
    switch(fs)
    {
    case 0:
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX+2;
    p[3].y=posY+1;
    break;
    case 1:
    p[0].x=posX+1;
    p[0].y=posY;
    p[1].x=posX;
    p[1].y=posY+1;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX;
    p[3].y=posY+2;
    break;

    }
}

void Tetrimino::pointsT(Point p[4], bool rot)
{
 int fs=!rot ? state: (state+1)%tabMaxStates[myType];
    switch(fs)
    {case 0:
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY;
    p[2].x=posX+2;
    p[2].y=posY;
    p[3].x=posX+1;
    p[3].y=posY+1;
    break;
    case 1:
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX;
    p[1].y=posY+1;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX;
    p[3].y=posY+2;
    break;
    case 2:
    p[0].x=posX+1;
    p[0].y=posY;
    p[1].x=posX;
    p[1].y=posY+1;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX+2;
    p[3].y=posY+1;
    break;
    case 3:
    p[0].x=posX+1;
    p[0].y=posY;
    p[1].x=posX;
    p[1].y=posY+1;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX+1;
    p[3].y=posY+2;
    break;

    }
}

void Tetrimino::pointsL(Point p[4], bool rot)
{
 int fs=!rot ? state: (state+1)%tabMaxStates[myType];
    switch(fs){
    case 0:
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX;
    p[1].y=posY+1;
    p[2].x=posX;
    p[2].y=posY+2;
    p[3].x=posX+1;
    p[3].y=posY+2;
    break;
    case 1:
    p[0].x=posX+2;
    p[0].y=posY;
    p[1].x=posX;
    p[1].y=posY+1;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX+2;
    p[3].y=posY+1;
    break;
    case 2:
     p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX+1;
    p[3].y=posY+2;
    break;
    case 3:
     p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY;
    p[2].x=posX+2;
    p[2].y=posY;
    p[3].x=posX;
    p[3].y=posY+1;
    break;
    }
}

void Tetrimino::pointsJ(Point p[4], bool rot)
{
 int fs=!rot ? state: (state+1)%tabMaxStates[myType];
    switch(fs){
    case 0:
    p[0].x=posX+1;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY+1;
    p[2].x=posX+1;
    p[2].y=posY+2;
    p[3].x=posX;
    p[3].y=posY+2;
    break;
    case 1:
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY;
    p[2].x=posX+2;
    p[2].y=posY;
    p[3].x=posX+2;
    p[3].y=posY+1;
    break;
    case 2:
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX+1;
    p[1].y=posY;
    p[2].x=posX;
    p[2].y=posY+1;
    p[3].x=posX;
    p[3].y=posY+2;
    break;
    case 3:
    p[0].x=posX;
    p[0].y=posY;
    p[1].x=posX;
    p[1].y=posY+1;
    p[2].x=posX+1;
    p[2].y=posY+1;
    p[3].x=posX+2;
    p[3].y=posY+1;
    break;
    }
}

void Tetrimino::points(Point p[4], bool rot)
{
    switch (myType)
    {
    case 0:
        pointsO(p, rot);
        break;
    case 1:
        pointsI(p, rot);
        break;
    case 2:
        pointsS(p, rot);
        break;
    case 3:
        pointsZ(p, rot);
        break;
    case 4:
        pointsT(p, rot);
        break;
    case 5:
        pointsL(p, rot);
        break;
    case 6:
        pointsJ(p, rot);
        break;
    }
}

void Tetrimino::nextState()
{
    state=(state+1)%tabMaxStates[myType];
}

void Tetrimino::moveLeft()
{
    printTet(false);
    --posX;
    ++posY;
    printTet(true);
}
void Tetrimino::moveRight()
{
    printTet(false);
    ++posX;
    ++posY;
    printTet(true);
}

void Tetrimino::moveDown(bool next)
{
    printTet(false);
    if(next)
        nextState();
    ++posY;
    printTet(true);
}


void Tetrimino::nextDown(bool next, Point p[4])
{
    points (p, next);

    for(int i=0; i<4; i++)
    {
        p[i].y=p[i].y+1;
    }


}
void Tetrimino::nextLeft(Point p[4])
{
    points(p, false);
    for(int i=0; i<4; ++i)
    {
        p[i].x=p[i].x-1;
        p[i].y=p[i].y+1;
    }

}

void Tetrimino::nextRight(Point p[4])
{
    points(p, false);
    for(int i=0; i<4; ++i)
    {
        p[i].x=p[i].x+1;
        p[i].y=p[i].y+1;
    }

}


