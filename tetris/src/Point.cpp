#include "Point.h"
//zmieniona klasa ComplexNumber z drugiego projektu
#include <ncurses.h>
#define BLOCK ' '|A_REVERSE
#define SPACE ' '

Point::Point()
{
    x=0;
    y=0;

}

Point::Point(int vx, int vy)
{
    x=vx;
    y=vy;
}

Point::~Point()
{
    //dtor
}

void Point::setXY(int vx, int vy)
{
     x=vx;
    y=vy;
}

void Point::print()
{
    move(y, x*2);
    addch(BLOCK);
    addch(BLOCK);

}

void Point::cleart()
{
    move(y, x*2);
    addch(SPACE);
    addch(SPACE);

}

bool Point::operator==(Point &p)
{
    return x==p.x && y==p.y;

}
bool Point::operator!=(Point &p)
{
    return x!=p.x || y!=p.y;

}

void Point::incY()
{
    ++y;
}
