#include "Game.h"
#include <iostream>
#include "MyFunctions.h"
#include "Point.h"
#include "Tetrimino.h"
#include "MyContainer.h"
#include <ncurses.h>


struct Game::GameImpl
{
    MyContainer <Point> listOfPoints;
    int score=0;
};


Game::Game() : impl_(new GameImpl)
{

}


Game::~Game()
{

}

void Game:: start()
{


    srand(time(NULL));
    initscr();
    rectangle(1, 21, 22 , 42);
    wresize(stdscr, 23,44 );
    keypad(stdscr, TRUE);
    timeout(1000);
    curs_set(0);
    noecho();
    cbreak();
    int i;
    char buf[20];
    bool endGame=false;
    while(!endGame)
    {
        bool ok=true;
        i=rand()%7;
        move(5,5);
        sprintf(buf, "Score: %d", impl_->score);
        printw(buf);
        Tetrimino *tet = new Tetrimino(TetType(i),15,2);
        Point p[4];
        tet->printTet(true);
        tet->nextDown(false, p);
        if(!canMoveDown(p))
        {
            ok=false;
            endGame=true;
            move(2,5);
            printw("Game over");
            int c=getch();

        }
        else
        {
            ok=true;
        }

        while(ok)
        {
            {
                int c=getch();
                switch(c)
                {
                case KEY_LEFT:
                    tet->nextLeft(p);
                    if(canMoveDown(p))
                    {
                        tet->moveLeft();
                    }
                    break;
                case KEY_RIGHT:
                    tet->nextRight(p);
                    if(canMoveDown(p))
                    {
                        tet->moveRight();
                    }
                    break;
                case KEY_UP:
                    tet->nextDown(true, p);
                    if(canMoveDown(p))
                    {
                        tet->moveDown(true);
                    }
                    break;
                default:
                    tet->nextDown(false, p);
                    if(canMoveDown(p))
                    {
                        tet->moveDown(false);
                    }
                    else
                    {
                        ok=false;
                    }

                }

            }
        }

        tet->points(p, false);
        impl_->listOfPoints.add(p[0]);
        impl_->listOfPoints.add(p[1]);
        impl_->listOfPoints.add(p[2]);
        impl_->listOfPoints.add(p[3]);
        delete tet;
        impl_->score+=clearSquares();

    }
    endwin();


}



bool Game::canMoveDown(Point p[4])
{
    for(int i=0; i<4; ++i)
    {
        if(p[i].y>21 || (p[i].x<11) || (p[i].x>20) )
        {
            return false;

        }
    }
    return !(impl_->listOfPoints.contains(p[0]) ||
             impl_->listOfPoints.contains(p[1]) ||
             impl_->listOfPoints.contains(p[2]) ||
             impl_->listOfPoints.contains(p[3])) ;
}

int Game::clearSquares()
{
    bool go=impl_->listOfPoints.first();

    while(go)
    {
        impl_->listOfPoints.get()->cleart();
        go=impl_->listOfPoints.hasNext();
    }

    int score=0;
    bool notEnd=true;
    int row=21;
    while (notEnd)
    {
        int countp=0;
        go=impl_->listOfPoints.first();
        while(go)
        {
            if( impl_->listOfPoints.get()->y==row)
                ++countp;
            go=impl_->listOfPoints.hasNext();
        }
        if(countp==10)
        {   ++score;
            go=impl_->listOfPoints.first();
            while(go)
            {
                if( impl_->listOfPoints.get()->y==row)
                {
                    impl_->listOfPoints.removecur();
                    go=impl_->listOfPoints.get()!=NULL;
                }
                else
                    go=impl_->listOfPoints.hasNext();
            }
            go=impl_->listOfPoints.first();
          while (go)
            {
                if(impl_->listOfPoints.get()->y<row)
                    impl_->listOfPoints.get()->incY();
                go=impl_->listOfPoints.hasNext();
            }

        }
        else if(countp==0)
            notEnd=false;
        else --row;
    }

    go=impl_->listOfPoints.first();

    while(go)
    {
        impl_->listOfPoints.get()->print();
        go=impl_->listOfPoints.hasNext();
    }
    return score;
}



