#ifndef TETRIMINO_H
#define TETRIMINO_H
#define NUMOFTETRIMINO 7
#include "Point.h"
#include <vector>

enum TetType {O, I, S, Z, T, L, J};
static int tabMaxStates[]= {1,2,2,2,4,4,4};

static int shapes[][4]=
{
    {0,0,0,0},
    {0,0,0,0},
    {2+4+8+16, 1+8+16+128,0,0},
    {1+2+16+32, 2+8+16+64,0,0},
    {1+2+4+16, 1+8+16+64, 2+8+16+32, 2+8+16+128},
    {1+8+64+128, 4+8+16+32, 1+2+16+128, 1+2+4+8},
    {2+16+64+128, 1+2+4+32, 1+2+8+64, 1+8+16+32}
};

class Tetrimino
{
public:
    Tetrimino(TetType type, int x, int y);
    virtual ~Tetrimino();
    void printTet (bool fillb);
    void points (Point p[4], bool rot);
    void pointsO (Point p[4], bool rot);
    void pointsI (Point p[4], bool rot);
    void pointsS (Point p[4], bool rot);
    void pointsZ (Point p[4], bool rot);
    void pointsT (Point p[4], bool rot);
    void pointsL (Point p[4], bool rot);
    void pointsJ (Point p[4], bool rot);
    void nextLeft(Point p[4]);
    void nextRight(Point p[4]);
    void print0(bool fillb);
    void nextDown(bool next, Point p[4]);
    void moveLeft();
    void moveRight();
    void moveDown(bool next);
protected:
private:
    void nextState();
    TetType myType;
    int state;
    int maxStates;
    int posX;
    int posY;
};

#endif // TETRIMINO_H
