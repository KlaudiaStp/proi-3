#ifndef TESTER_H
#define TESTER_H
#include "MyContainer.h"
#include "Point.h"
#include "Tetrimino.h"
#include <memory>


class Tester
{
    public:
        Tester();
        virtual ~Tester();
        void runTest();
    protected:
    private:
        void testTetO();
        void testTetI();
        void testTetS();
        void testTetZ();
        void testTetT();
        void testTetL();
        void testTetJ();
        struct TesterImpl;
        std::unique_ptr<TesterImpl> impl_;
};

#endif // TESTER_H
