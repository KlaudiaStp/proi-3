#ifndef MYCONTAINER_H
#define MYCONTAINER_H
#include <cstddef>
//zmieniona klasa ListofComplexNumbers z drugiego projektu

template <class T>
struct Element
{

    T t;
    Element *next;
    Element *prev;

};

template <class T>
class MyContainer
{
public:
    MyContainer();
    virtual ~MyContainer();
    void add(T &vt);
    bool first();
    T* get();
    bool hasNext();
    bool update(T &t);
    bool removecur();
    bool contains(T &t);
protected:
private:
    Element<T> *current;
    Element<T> *head;
    Element<T> *tail;
    int size;
};

template <class T>
MyContainer <T>::MyContainer()
{
    size=0;
    head=NULL;
    tail=NULL;
    current=head;
}

template <class T>
MyContainer <T>::~MyContainer()
{
    Element<T> *tmp;
    while(head!=NULL)
    {
        tmp=head->next;
        delete head;
        head=tmp;
    }
}
template <class T>
void MyContainer<T>::add(T &vt)
{
    Element<T> *tmp = new Element<T>;
    tmp->t= vt;
    tmp->prev=tail;
    tmp->next=NULL;
    tail=tmp;
    ++size;
    if(tmp->prev!=NULL)
    {
        tmp->prev->next=tmp;
    }
    else
    {
        head=tmp;
    }


}

template <class T>
bool MyContainer<T>::first()
{
    current=head;
    return current!=NULL;
}

template <class T>
T* MyContainer<T>::get()
{
   if(current!=NULL)
    {
       /* T* tmp= new T();
        *tmp=current->t;
        return tmp;*/
        return &(current->t);
    }

    else
        return NULL;
}

template <class T>
bool MyContainer<T>::hasNext()
{
    if(current==NULL)
        return false;
    else
    {
        current=current->next;
        return current!=NULL;
    }
}

template <class T>
bool MyContainer<T>::update(T &t)
{
    if(current!=NULL)
    {
        current->t=t;
        return true;
    }
    else
        return false;
}

template <class T>
bool MyContainer<T>::removecur()
{
    if(current==NULL)
    {
        return false;
    }
    else
    {
        Element<T> *tmp=current;
        current=current->next;
        if(tmp!=head)
            tmp->prev->next=tmp->next;
        if(current!=NULL)
            current->prev=tmp->prev;
        if(tmp==head)
            head=current;
        if(tmp==tail)
            tail=tmp->prev;
        --size;
        delete tmp;
        return true;

    }
}

template <class T>
bool MyContainer<T>::contains(T &t)
{
    Element<T> *p=head;
    bool go=true;
    while (go&&(p!=NULL))
    {
        if(p->t==t)
        {
            go=false;
        }
        else
        {
            p=p->next;
        }
    }
    return !go;
}

#endif // MYCONTAINER_H
