#ifndef GAME_H
#define GAME_H
#include <memory>
#include "Point.h"


class Game
{
    public:
        Game();
        virtual ~Game();
        void start();
        bool canMoveDown (Point p[4]);
        int clearSquares ();
    protected:
    private:
        struct GameImpl;
        std::unique_ptr<GameImpl> impl_;

};

#endif // GAME_H
