#include <iostream>
#include <ncurses.h>
#include <cstdlib>
#include "MyFunctions.h"
#include "Game.h"
#include "Tester.h"

using namespace std;

int main()
{
   // Tester *test= new Tester();
    //test->runTest();

    Game *g= new Game();
    g->start();
    //delete test;
    delete g;

    return 0;
}
